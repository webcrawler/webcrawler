Alunos:
	Eduardo Vinícius dos Santos	10102165	
	Lúcio Leal Bastos			11100356

Descrição:

Utilizamos Java para a criação do trabalho, pela grande documentação das APIs.
O programa busca recursivamente em uma lista de links (absolutos), monta os
diretórios de páginas como no servidor e faz o download e extração dos objetos
(le-se por objeto tudo que não seja página). Este processo é feito até todas as 
listas serem consumidas.

Quando o link extraído é um https, no momento que ele for consumido da lista é 
exibido informações sobre o nome da autoridade certificadora e sobre o dono do 
certificado, assim como a URL.
Caso o certificado seja autoassinado, essa informação é exibida juntamente com 
a URL acessada.

Problemas encontrados:
- Não encontramos uma forma eficiente de expressão regular para pegar links relativos.
- Não conseguimos implementar threads
- Links com Queries podem apresentar problemas (jogam exceções)

Utilização:

./chmod+x executeme.sh
./executeme.sh profundidade url(com "/" no final)