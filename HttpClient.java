package webcrawler;

import java.io.*;
import java.net.URL;
import java.security.Principal;
import java.util.Arrays;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HttpsURLConnection;
import static webcrawler.ManipulaArquivo.manipulaArquivo;

public class JavaHttps {

    public static void javaHttps(String caminhoAbsoluto, int p)
            throws Exception {
        System.out.println("Arquivo HTTPS: URL = " + caminhoAbsoluto);

        System.out.println("[JavaHttps] URL HTTPS: " + caminhoAbsoluto + "Profundidade " + p);
        String httpsURL = caminhoAbsoluto;
        //String httpsURL = "https://www.facebook.com";
        //https://www.pcwebshop.co.uk/
        
        URL myurl = new URL(httpsURL);
        HttpsURLConnection con = (HttpsURLConnection) myurl.openConnection();

        String diretorio = new String();
        diretorio = caminhoAbsoluto.replace("https://", "");
        File path = new File(diretorio);
        path.mkdirs();
        File arquivo = new File(path, "index.html");
        InputStream ins = con.getInputStream();
        InputStreamReader isr = new InputStreamReader(ins);
        BufferedReader in = new BufferedReader(isr);

        String inputLine;
        String serverCertificate = new String();

        serverCertificate = Arrays.toString(con.getServerCertificates());
        //cria um padrão para buscar a autoridade
        Pattern padrao = Pattern.compile("(Issuer):.*(O=).*,");

        //Casa o padrão com a linha analisada
        Matcher autoridade = padrao.matcher(serverCertificate);
        String resultado = new String();

        if (autoridade.find()) {
            //Extrai o link da linha
            MatchResult matchResult = autoridade.toMatchResult();
            resultado = matchResult.group();
        }
        resultado = resultado.replaceAll("\"", "");
        
        System.out.println("\n\nAutoridade certificadora: " + resultado.substring(resultado.indexOf("O=") + 2, resultado.indexOf(", L=")) + "\n\n");

        //System.out.println("SERVERMTOTT"+Arrays.toString(con.getServerCertificates()));
        String owner = con.getPeerPrincipal().toString();//extrair o dono do certificado
        //owner = owner.replaceAll(",", "\n");
        owner = owner.replaceAll("\"","");
        owner = owner.replace("EMAILADDRESS=","Email: ");
        owner = owner.replace("OU=","Autoridade: ");
        owner = owner.replace("CN=", "Domínio: ");
        owner = owner.replace("O=", "Nome: ");
        owner = owner.replace("STREET=", "Endereço: ");
        owner = owner.substring(0,owner.indexOf(", L="));
        System.out.println("\n\n" + owner + "\n\n");
        try {
            //o metodo passa para o arquivo o conteudo do buffer
            try (BufferedWriter outFile = new BufferedWriter(new FileWriter(arquivo))) {
                 while ((inputLine = in.readLine()) != null) {
                    outFile.write(inputLine+"\n");
                }
            }
        } catch (FileNotFoundException exception) {
            //System.out.println("Arquivo nao encontrado.");
        } catch (IOException exception) {
            //System.out.println("Erro de I/O: " + exception);
        }
        in.close();
        manipulaArquivo(arquivo, p);
    }
}
